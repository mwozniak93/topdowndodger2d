﻿using UnityEngine;
using System.Collections;
using System.Linq;

[RequireComponent (typeof(MovementComponent))]
public abstract class BasicEnemyController : MonoBehaviour,IEnemyController
{
	MovementComponent movementComponent;
	ClockManager clockManager;
	[HideInInspector]
	public PoolManager poolManager;
	// Use this for initialization
	public virtual void Awake ()
	{
		movementComponent = GetComponent<MovementComponent> ();
	}


	public virtual void Start ()
	{
		clockManager = ManagersContainer.Instance.GetManager<ClockManager> ();
		poolManager = ManagersContainer.Instance.GetManager<PoolManager> ();

	}

	public void Init (CreatureModel enemyModel)
	{
		GetComponents<IEnemyComponent> ().ToList ().ForEach (x => x.Init (enemyModel));
	}
	// Update is called once per frame
	void Update ()
	{

		movementComponent.Move (new Vector3 (0, clockManager.EnemyClock * (-0.2f), 0));

	}

	void OnTriggerEnter2D (Collider2D coll)
	{
		TriggerBehavior (coll);
	}
	public virtual void TriggerBehavior(Collider2D coll){
		//		Debug.Log ("coll ; " + coll.name);
		if (coll.CompareTag (Constants.WALL_BOTTOM_TAG)) {
			gameObject.SetActive (false);
			//			poolManager.Despawn (GetComponent<IPoolable> ().PoolKey, GetComponent<IPoolable> ().Name);
		}
	}
}

