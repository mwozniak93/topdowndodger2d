﻿using UnityEngine;
using System.Collections;

public class BlueEnemyController : BasicEnemyController
{
	GameManager gameManager;
	HealthComponent healthComponent;
	[SerializeField]
	PoolObject wallCollisionExplosionTemplate;
	CameraShakeManager cameraShakeManager;

	public override void Awake ()
	{
		
		base.Awake ();
		healthComponent = GetComponent<HealthComponent> ();
		healthComponent.OnDeath += OnDeath; 

	}

	public override void Start ()
	{
		base.Start ();
		gameManager = ManagersContainer.Instance.GetManager<GameManager> ();
		cameraShakeManager = ManagersContainer.Instance.GetManager<CameraShakeManager> ();

	}

	private void OnDeath ()
	{
		gameManager.AddScore (1);
	}

	public override void TriggerBehavior (Collider2D coll)
	{
		if (coll.CompareTag (Constants.WALL_BOTTOM_TAG)) {
			GameObject wallCollisionExplosion = poolManager.Spawn (wallCollisionExplosionTemplate.Name);
			wallCollisionExplosion.transform.position = transform.position;
			wallCollisionExplosion.SetActive (true);
			cameraShakeManager.Shake ();
			gameManager.AddScore (-1);
		}
		base.TriggerBehavior (coll);
	}
}

