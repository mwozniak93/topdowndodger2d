﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerController : MonoBehaviour
{

	SuperSkillComponent superSkillComponent;
	MovementComponent movementComponent;
	HitComponent hitComponent;
	[SerializeField]
	GameObject trailsEffect, tail;
	bool isEnabled;
	BoxCollider2D boxCollider2D;
	WavesManager wavesManager;

	void Awake ()
	{
		boxCollider2D = GetComponent<BoxCollider2D> ();
		hitComponent = GetComponent<HitComponent> ();
		movementComponent = GetComponent<MovementComponent> ();
		superSkillComponent = GetComponent<SuperSkillComponent> ();
		superSkillComponent.OnSkillStart += OnSkillStart;
		superSkillComponent.OnSkillEnd += OnSkillEnd;
	}

	void Start ()
	{

		wavesManager = ManagersContainer.Instance.GetManager<WavesManager> ();
	}

	public int RequiredEnergy {
		get{ return superSkillComponent.RequiredEnergy; }
	}

	public int CurrentEnergy {
		get{ return superSkillComponent.CurrentEnergy; }
	}

	private void OnSkillStart ()
	{
		DisablePlayer ();
		wavesManager.StopWave ();
	}

	private void OnSkillEnd ()
	{
		EnablePlayer ();
		wavesManager.StartWave ();
	}

	public void EnablePlayer ()
	{
		isEnabled = true;
	}

	public void AddEnergy (int val)
	{
	
		superSkillComponent.AddEnergy (val);
	}

	public void DisablePlayer ()
	{
		isEnabled = false;
		movementComponent.Move (Vector3.zero);
	}
	// Update is called once per frame
	void Update ()
	{
		trailsEffect.transform.position = tail.transform.position;
		if (Input.GetButtonUp ("Jump") && isEnabled) {
			superSkillComponent.UseSkill (0f);
		}

//		transform.position += move * speed * Time.deltaTime;
	}

	void FixedUpdate ()
	{
		if (isEnabled) {
			Vector3 moveDirection = new Vector3 (Input.GetAxis ("Horizontal"), Input.GetAxis ("Vertical"), 0);
			movementComponent.Move (moveDirection);
		}
	}
	

}

