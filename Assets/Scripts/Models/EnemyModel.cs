﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[CreateAssetMenu (fileName = "Enemy", menuName = "Create Enemy", order = 1)]
public class CreatureModel : ScriptableObject
{
	public GameObject Prefab;
	public float MaxHealth;
	public float Acceleration;

}
	