﻿using UnityEngine;
using System.Collections;

public class BackgroundScroller : MonoBehaviour
{
	private ClockManager clockManager;
	private Material mainMaterial;
	// Use this for initialization
	void Start ()
	{
		mainMaterial = GetComponent<Renderer> ().material;
		clockManager = ManagersContainer.Instance.GetManager<ClockManager> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
		Vector2 offset = new Vector2 (0, Time.time * 0.25f);
		mainMaterial.mainTextureOffset = offset;
	}
}

