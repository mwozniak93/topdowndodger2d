﻿using UnityEngine;
using System.Collections;
using System;

public class SuperSkillComponent : MonoBehaviour
{
	ClockManager clockManager;
	[SerializeField]
	private GameObject timePanel;
	[SerializeField]
	PoolObject superBulletTemplate, castEffectTemplate;
	PoolManager poolManager;

	public event Action OnSkillStart = delegate {};
	public event Action OnSkillEnd = delegate {};
	public event Action<int,int> OnEnergyChange = delegate {};

	[SerializeField]
	private int requiredEnergy = 5;

	public int RequiredEnergy {
		get{ return requiredEnergy; }
		private set{ requiredEnergy = value; }
	}

	private int energy;

	public int CurrentEnergy {
		get{ return energy; }
		private set{ energy = value; }
	}

	public void AddEnergy (int value)
	{
		if ((value > 0 && (CurrentEnergy < RequiredEnergy && CurrentEnergy >= 0)) || (value < 0 && CurrentEnergy > 0)) {
			CurrentEnergy += value;
			OnEnergyChange.Invoke (CurrentEnergy, RequiredEnergy);
		}

	}
	// Use this for initialization
	void Start ()
	{
		poolManager = ManagersContainer.Instance.GetManager<PoolManager> ();
		clockManager = ManagersContainer.Instance.GetManager<ClockManager> ();
	}

	public void UseSkill (float timeValue)
	{
		if (CurrentEnergy < requiredEnergy) {
			return;
		}
		CurrentEnergy = 0;
		OnEnergyChange.Invoke (CurrentEnergy, RequiredEnergy);
		StartCoroutine (UseSkillCorutine (timeValue));
	}

	public IEnumerator UseSkillCorutine (float timeValue)
	{
		OnSkillStart.Invoke ();
		GameObject castEffect = poolManager.Spawn (castEffectTemplate.Name);
		castEffect.transform.position = transform.position;
		castEffect.SetActive (true);
		timePanel.SetActive (true);
		clockManager.ChangeClockValue (timeValue, 0.25f);
		yield return new WaitForSeconds (2.3f);
		GameObject superBullet = poolManager.Spawn (superBulletTemplate.Name);
		superBullet.transform.position = transform.position;
		superBullet.SetActive (true);
		yield return new WaitForSeconds (1f);
		clockManager.ChangeClockValue (1f, 0.25f);
		timePanel.SetActive (false);
		castEffect.SetActive (false);
		superBullet.SetActive (false);
		OnSkillEnd.Invoke ();
	}
}

