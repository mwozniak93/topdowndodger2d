﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class HealthComponent : MonoBehaviour, IEnemyComponent
{

	[SerializeField]
	float currentAmount;
	public event Action OnDeath = delegate {};
	[SerializeField]
	PoolObject hitEffectTemplate;
	PoolManager poolManager;
	void Start(){
		poolManager = ManagersContainer.Instance.GetManager<PoolManager> ();

	}

	public void TakeDamage (float amount)
	{
		currentAmount -= amount;
		if (currentAmount <= 0) {
			OnDeath.Invoke ();
			GameObject effect = poolManager.Spawn (hitEffectTemplate.Name);
			effect.transform.position = transform.position;
			effect.SetActive (true);
			gameObject.SetActive (false);
		}

	}


	#region IEnemyComponent implementation
	public void Init (CreatureModel enemyModel)
	{
		currentAmount = enemyModel.MaxHealth;
	}
	#endregion
}