﻿using UnityEngine;
using System.Collections;

public class DeactivatorComponent : MonoBehaviour
{
	[SerializeField]
	private float timeToDisable;
	// Use this for initialization
	void OnEnable ()
	{
		StartCoroutine (Disable ());

	}

	IEnumerator Disable ()
	{

		yield return new WaitForSeconds (timeToDisable);
		gameObject.SetActive (false);
	}
}

