﻿using UnityEngine;
using System.Collections;


public class MovementComponent : MonoBehaviour, IEnemyComponent
{

	[SerializeField]
	private float acceleration;
	Rigidbody2D rigidbody2D;

	void Awake(){
		rigidbody2D = GetComponent<Rigidbody2D> ();
	}
	void Start ()
	{
//		_centre = transform.position;
	}

	public void Move (Vector3 direction)
	{
		rigidbody2D.velocity = direction * acceleration * Time.deltaTime;
//		transform.Translate(direction * acceleration * Time.deltaTime);
	}
		


	#region IEnemyComponent implementation
	public void Init (CreatureModel enemyModel)
	{
		this.acceleration = enemyModel.Acceleration;
	}
	#endregion
}