﻿using UnityEngine;
using System.Collections;
using System;

public class HitComponent : MonoBehaviour
{

	public event Action<float> OnCollObjectHit = delegate {};

	void OnTriggerEnter2D(Collider2D coll){
	
		if (coll.CompareTag(Constants.CREATURE_TAG)) {

			coll.GetComponent<HealthComponent> ().TakeDamage (1);
			OnCollObjectHit.Invoke (1);

		}
	}
}

