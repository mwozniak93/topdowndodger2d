﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudPanel : UiElementBase
{

	private GameManager gameManager;
	private WavesManager wavesManager;
	[SerializeField]
	private Text scoreLabel, energyLabel;
	[SerializeField]
	private Image energyBar;
	[SerializeField]
	private SuperSkillComponent playerSkillComponent;

	void Start ()
	{
		wavesManager = ManagersContainer.Instance.GetManager<WavesManager> ();
		gameManager = ManagersContainer.Instance.GetManager<GameManager> ();
		gameManager.OnScoreChange += OnScoreChange;
		playerSkillComponent.OnEnergyChange += OnEnergyChange;
		OnScoreChange (0);
		OnEnergyChange (gameManager.playerController.CurrentEnergy, gameManager.playerController.RequiredEnergy);
	}

	private void OnEnergyChange (int newValue, int requiredEnergy)
	{
	
//		Debug.Log("value + requireednergy
		if (newValue < 0) {
			return;
		}
		energyBar.fillAmount = (float)newValue / (float)requiredEnergy;
		energyLabel.text = string.Format ("{0}/{1}", newValue, requiredEnergy);
	}

	private void OnScoreChange (int score)
	{
		scoreLabel.text = string.Format ("Score\n<size=45%>{0}</size>", score);

	}


}
