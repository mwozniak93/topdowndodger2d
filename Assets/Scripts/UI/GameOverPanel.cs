﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverPanel : UiElementBase
{

	[SerializeField]
	private Button restartGameButton;
	private GameManager gameManager;
	[SerializeField]
	private Text summaryLabel;
	[SerializeField]
	HealthComponent playerHealthComponent;

	void Awake ()
	{
		playerHealthComponent.OnDeath += OnPlayerDefeated;
		restartGameButton.onClick.AddListener (OnRestartGameButtonClick);
	}

	void OnPlayerDefeated ()
	{
		Debug.Log ("OnPlayerDefeated ");
		UiDisplayer.Show ();
		summaryLabel.text = string.Format ("Congratulations! You got {0} points!", gameManager.Score);
	}

	void Start ()
	{
		gameManager = ManagersContainer.Instance.GetManager<GameManager> ();
	}


	public void OnRestartGameButtonClick ()
	{
		UiDisplayer.Hide ();
		gameManager.RestartGame ();
	}
}

