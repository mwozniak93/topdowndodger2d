﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUiDisplayer
{
	void Show ();
	void Hide ();
}
[RequireComponent(typeof(UiElementDisplayer))]
public abstract class UiElementBase : MonoBehaviour
{
	public UiElementDisplayer UiDisplayer;

	void Awake(){
		UiDisplayer = GetComponent<UiElementDisplayer> ();
	}
	

	public void Show ()
	{
		UiDisplayer.Show ();
//		gameObject.SetActive (true);
	}

	public void Hide ()
	{
		UiDisplayer.Hide ();
//		gameObject.SetActive (false);
	}
}
