﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(CanvasGroup))]
public class UiElementDisplayer : MonoBehaviour, IUiDisplayer
{
	private CanvasGroup canvasGroup;

	void Awake ()
	{
		canvasGroup = GetComponent<CanvasGroup> ();
	}

	#region IUiPanelShower implementation

	public void Show ()
	{
		canvasGroup.interactable = true;
		canvasGroup.blocksRaycasts = true;
		StartCoroutine (FadeIn (0f, 1f));
	}

	public void Hide ()
	{
		canvasGroup.interactable = false;
		StartCoroutine (FadeOut (1f, 0f));

	}

	#endregion
	IEnumerator FadeIn (float valFrom, float valTo)
	{
		while (canvasGroup.alpha <= valTo) {
			yield return new WaitForSeconds (0.01f);
			canvasGroup.alpha += 0.02f;
		}
	}
	IEnumerator FadeOut (float valFrom, float valTo)
	{
		while (canvasGroup.alpha > valTo) {
			yield return new WaitForSeconds (0.01f);
			canvasGroup.alpha -= 0.02f;
		}
	}
}

