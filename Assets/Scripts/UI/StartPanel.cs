﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartPanel : UiElementBase
{
	[SerializeField]
	private Button startGameButton;
	private GameManager gameManager;

	void Awake ()
	{
		startGameButton.onClick.AddListener (OnStartGameButtonClick);
	}

	void Start ()
	{
		gameManager = ManagersContainer.Instance.GetManager<GameManager> ();
	}


	public void OnStartGameButtonClick ()
	{
		UiDisplayer.Hide ();
		gameManager.StartGame ();
	}
}

