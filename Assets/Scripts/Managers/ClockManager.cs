﻿using UnityEngine;
using System.Collections;

public class ClockManager : MonoBehaviour
{
	[SerializeField]
	private float enemyClock = 1f;

	public float EnemyClock{ get { return enemyClock; } private set { enemyClock = value; } }

	public void ChangeClockValue (float newValue, float duration)
	{
		StartCoroutine (ChangeClockValueCorutine (newValue, duration));
	}

	private IEnumerator ChangeClockValueCorutine (float newValue, float duration)
	{
		for (float t = 0f; t < duration; t += Time.deltaTime) {
			EnemyClock = Mathf.Lerp (EnemyClock, newValue, t / duration);
			yield return null;
		}
		EnemyClock = newValue;
	}

}

