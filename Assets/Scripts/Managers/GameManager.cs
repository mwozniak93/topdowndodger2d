﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
	private WavesManager wavesManager;
	[SerializeField]
	public PlayerController playerController;
	[SerializeField]
	HealthComponent playerHealthComponent;

	public event Action<int> OnScoreChange = delegate {};

	private bool isGameStopped = false;

	[SerializeField]
	private int score;

	public int Score {
		get{ return score; }
		private set{ score = value; }
	}

	void Start ()
	{
		playerController.DisablePlayer ();
		wavesManager = ManagersContainer.Instance.GetManager<WavesManager> ();
		playerHealthComponent.OnDeath += StopGame;
	}


	public void AddScore (int value)
	{
		if (!isGameStopped) {
			playerController.AddEnergy (value);
			Score += value;
			OnScoreChange.Invoke (Score);
		}
	}

	private void StopGame ()
	{
		isGameStopped = true;
		wavesManager.StopWave ();
		playerController.DisablePlayer ();
	}

	public void StartGame ()
	{
		wavesManager.StartWave ();
		playerController.EnablePlayer ();
	}

	public void RestartGame ()
	{
		SceneManager.LoadScene ("MainScene");
	}
}
