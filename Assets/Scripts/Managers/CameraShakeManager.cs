﻿using UnityEngine;
using System.Collections;

public class CameraShakeManager : MonoBehaviour
{

	[SerializeField]
	Transform targetCamera;
	float posY;
	float shakeAmount = 1;
	bool flip;


	public void Shake ()
	{
		StartCoroutine (ShakeCorutine ());
	}

	IEnumerator ShakeCorutine ()
	{
		posY = shakeAmount;

		while (Mathf.Abs (posY) > 0.01f) {
			posY = Mathf.Abs (posY) - 0.1f;

			if (flip) {
				posY *= -1;
			}

			flip = !flip;
			Vector3 tempPos = targetCamera.localPosition;
			tempPos.y = posY;
			targetCamera.localPosition = tempPos;

			yield return null;
		}
	}
}

