﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu (fileName = "Enemies Collection", menuName = "Create Enemies Collection", order = 1)]
public class EnemiesCollection : ScriptableObject
{

	public List<CreatureModel> Enemies;
}

