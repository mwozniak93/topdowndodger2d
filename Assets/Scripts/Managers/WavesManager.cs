﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System.Linq;
using System;



public class WavesManager : MonoBehaviour
{
	[SerializeField]
	Transform SpawnPoints;
	[SerializeField]
	Transform leftSpawnPoint, rightSpawnPoint;
	PoolManager poolManager;
	[SerializeField]
	EnemiesCollection enemiesCollection;
	ClockManager clockManager;
	public event Action OnNewWave = delegate {};
	[SerializeField]
	float waveRate = 0.65f;

	void Start ()
	{
		clockManager = ManagersContainer.Instance.GetManager<ClockManager> ();
		poolManager = ManagersContainer.Instance.GetManager<PoolManager> ();
//		StartCoroutine (CreateWave ());
	}

	public void StartWave ()
	{
		StartCoroutine (CreateWave ());

	}

	public void StopWave ()
	{
		StopAllCoroutines ();

	}



	IEnumerator CreateWave ()
	{
		yield return new WaitForSeconds (waveRate);
		int randomEnemy = UnityEngine.Random.Range (0, enemiesCollection.Enemies.Count);
		CreatureModel enemyModel = enemiesCollection.Enemies [randomEnemy];
		GameObject enemyToSpawn = enemiesCollection.Enemies [randomEnemy].Prefab; 
		GameObject newEnemy = poolManager.Spawn (enemyToSpawn.name);
		newEnemy.gameObject.SetActive (true);
//		newEnemy.transform.position = SpawnPoints.GetChild (UnityEngine.Random.Range (0, SpawnPoints.childCount)).position;
		float randomX = UnityEngine.Random.Range(leftSpawnPoint.position.x, rightSpawnPoint.position.x);
		newEnemy.transform.position = new Vector3 (randomX, leftSpawnPoint.position.y, leftSpawnPoint.position.z);
		OnNewWave.Invoke ();
		IEnemyController enemyController = newEnemy.GetComponent<IEnemyController> ();
		enemyController.Init (enemyModel);
		StartCoroutine (CreateWave ());
	}


}