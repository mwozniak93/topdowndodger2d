﻿using UnityEngine;
using System.Collections;

public interface IEnemyController 
{
	void Init(CreatureModel enemyModel);
}


public interface IEnemyComponent 
{
	void Init(CreatureModel enemyModel);
}

