﻿using UnityEngine;

using System.Collections.Generic;

	public interface IPool
	{
		GameObject Spawn (string key);

		GameObject Despawn (string key, string name, bool detach = false);

		void Init (List<GameObject> poolables, bool isAr=false);
	}
