﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PoolFeeder : MonoBehaviour
{
	PoolManager poolManager;
	[SerializeField]
	private List<GameObject> poolObjects;
	// Use this for initialization
	void Start ()
	{
		poolManager = ManagersContainer.Instance.GetManager<PoolManager> ();
		Init ();
	}

	private void Init ()
	{
		poolManager.Init (poolObjects);
	}


}
